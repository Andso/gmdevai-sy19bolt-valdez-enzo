﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawn : MonoBehaviour
{
    // Start is called before the first frame update


    public GameObject Monster;
    public GameObject Hero;

    GameObject[] agents;


    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("agent");
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray.origin, ray.direction, out hit))
            {

                Instantiate(Monster, hit.point, Monster.transform.rotation);
                foreach(GameObject a in agents)
                {
                    a.GetComponent<AIControl>().DetectNewObstacle(hit.point);

                }

            }

        }
        if (Input.GetMouseButtonDown(1))
        {

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray.origin, ray.direction, out hit))
            {

                Instantiate(Hero, hit.point, Hero.transform.rotation);
                foreach (GameObject a in agents)
                {
                    a.GetComponent<AIControl>().FlockNewObstacle(hit.point);

                }

            }

        }
    }






}
