﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour
{

    public Transform player;
    public float speed = 5;
    public float rotspeed = 5;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 lookatgoal = new Vector3(player.position.x,
                                        this.transform.position.y,
                                        player.position.z);

        Vector3 direction = lookatgoal - transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                    Quaternion.LookRotation(direction),
                                                    Time.deltaTime * rotspeed);

        if(Vector3.Distance(lookatgoal, transform.position) > 1)
        {
            transform.Translate(0, 0, speed * Time.deltaTime);
        }

    }
}
