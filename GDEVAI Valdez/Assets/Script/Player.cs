﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{


    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float Horizontaltransition = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        float Verticaltransition = Input.GetAxis("Vertical") * speed * Time.deltaTime;

        this.transform.position += new Vector3(Horizontaltransition, 0, Verticaltransition);
    }
}
